 //store variables from forms - inputs
 var contactname = document.forms['form']['form-name'];
 var email = document.forms['form']['email'];
 var password = document.forms['form']['password'];
 var repeat_pass = document.forms['form']['repeat-pass'];
 var username = document.forms['form']['username'];
 var message = document.forms['form']['message'];

//store variables from forms - hidden paragraphs
 var email_error = document.getElementById('email-error');
 var name_error = document.getElementById('name-error');
 var pass_error = document.getElementById('pass-error');
 var username_error = document.getElementById('username-error');
 var confirm_error = document.getElementById('confirm-error');
 var message_error = document.getElementById('message-error');



//singin page validation
function validatesignin(){
  
     if(email.value.length==0){
         email.focus();
         email_error.style.visibility = "visible";
         return false;
     }
     if(password.value.length <8){
         password.focus();
         pass_error.style.visibility ="visible";
         return false;
     }

  
 }
 //signup page validation
function validatesignup(){

        if(username.value.length==0){
            username.focus();
            username_error.style.visibility = "visible";
            return false;
        }
        if(email.value.length==0){
            email.focus();
            email_error.style.visibility = "visible";
            return false;
        }
        if(password.value.length <8){
            password.focus();
            pass_error.style.visibility ="visible";
            return false;
        }
        if(repeat_pass.value != password.value){
            repeat_pass.focus();
            confirm_error.style.visibility ="visible";
            return false;
        }

}

//contact page validation
function validateContact(){

        if(contactname.value.length==0){
            contactname.focus();
            name_error.style.visibility = "visible";
            return false;
        }
        if(email.value.length==0){
            email.focus();
            email_error.style.visibility = "visible";
            return false;
        }
        if(message.value.length==0){
        message.focus();
        message_error.style.visibility="visible";
        return false;
    }
}
